<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/9/2017
 * Time: 10:12 AM
 */

namespace app\controllers;


use app\models\forms\LoginForm;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class AuthController extends Controller
{


    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}