<?php

namespace app\controllers;


use app\components\category\CategoryFinder;
use app\components\project\ProjectFinder;
use app\components\projectListComponent\ProjectListAction;
use app\models\base\Article;
use app\models\base\Category;
use app\models\base\Project;
use app\models\frontend\FrontProject;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class ProjectController extends Controller
{

    public function behaviors()
    {
        return [
            [
                'class' => AccessControl::className(),
                'only' => ['edit', 'new', 'remove'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['edit', 'new', 'remove'],
                        'roles' => ['editAndAddProject'],
                    ]
                ],

            ],
        ];
    }

    public function actionList(){

        $categoryFinder = new CategoryFinder();
        $projectFinder = new ProjectFinder(Yii::$app->request->post('categories'));



        $phpProvider = [
            'startCategories' => $categoryFinder->getAllArrayCategories(),
            'startProjects' => $projectFinder->getStartProjects(),
            'canEditAndAddProject' => Yii::$app->user->can('editAndAddProject'),
        ];

        return $this->render('list', [
            'phpProvider' => json_encode($phpProvider, JSON_UNESCAPED_UNICODE)
        ]);
    }

    public function actionGetProjectsJson(){

        $projectFinder = new ProjectFinder(Yii::$app->request->post('categories'));

        $page = Yii::$app->request->post('page', 0);

        return json_encode($projectFinder->getProjectsByCategoryIds($page), JSON_UNESCAPED_UNICODE);
    }
    public function actionSearchProject($query){
        $projects = Project::find()
            ->select(['id', 'name'])
            ->orderBy(['rating' => SORT_DESC])
            ->where(['like', 'name', $query])
            ->limit(5)
            ->asArray()
            ->all();

        return json_encode($projects, JSON_UNESCAPED_UNICODE);
    }

    public function actionView($id)
    {
        $project = Project::findOne($id);

        if(!$project)
            throw new BadRequestHttpException('Проект не найден');

        Article::attachArticleTextAndDescriptionToProject($project);

        Category::attachCategoryDataToProject($project);

        return $this->render('view', [
            'project' => $project,
            'articleToThisProjectExist' => Article::find()
                ->where(['target_id' => $project->id])
                ->andWhere(['target_type' => Article::TARGET_TYPE_PROJECT])->exists(),
            'canEditAndAddProject' => Yii::$app->user->can('editAndAddProject'),
            'canEditAndAddArticle' => Yii::$app->user->can('editAndAddArticle'),
        ]);
    }

    public function actionEdit($id)
    {
        $projectEditForm = FrontProject::findOne($id);

        if (!$projectEditForm)
            throw new BadRequestHttpException('Проект не найден.');

        $projectEditForm->scenario = FrontProject::SCENARIO_EDIT_FORM;
        if (
            $projectEditForm->load(Yii::$app->request->post()) &&
            $projectEditForm->validate() &&
            $projectEditForm->save()
        ){
            $projectEditForm->updateSelectCategory();
            return $this->redirect(Url::to(['project/view', 'id' => $projectEditForm->id]));
        }


        return $this->render('edit', [
            'projectEditForm' => $projectEditForm,
            'projectRemoveForm' => new FrontProject(['scenario' => FrontProject::SCENARIO_REMOVE_FORM]),
            'projectId' => $id,
        ]);
    }

    public function actionRemove()
    {
        $projectRemoveForm = new FrontProject(['scenario' => FrontProject::SCENARIO_REMOVE_FORM]);

        if ($projectRemoveForm->load(Yii::$app->request->post()) &&
            $projectRemoveForm->validate() &&
            $projectRemoveForm->deleteByThisId()
        )
            return $this->redirect(Yii::$app->urlManager->createUrl('project/list'));
        else
            throw new BadRequestHttpException('Не удалось произвести удаление.');
    }

    public function actionNew()
    {
        $projectNewForm = new FrontProject(['scenario' => FrontProject::SCENARIO_NEW_FORM]);

        if ($projectNewForm->load(Yii::$app->request->post()) &&
            $projectNewForm->validate() &&
            $projectNewForm->save()
        ){
            $projectNewForm->updateSelectCategory();
            return $this->redirect(Yii::$app->urlManager->createUrl(['project/view', 'id' => $projectNewForm->id]));
        }

        return $this->render('new', [
            'projectNewForm' => $projectNewForm,
        ]);
    }
}
