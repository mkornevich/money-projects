<?php

namespace app\controllers;

use app\models\base\Article;
use app\models\base\Project;
use vova07\imperavi\actions\GetAction;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'images-get', 'image-upload'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['images-get', 'image-upload'],
                        'allow' => true,
                        'roles' => ['loadImages'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],

            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' =>  $_ENV['storageUrl'] . 'images/', // Directory URL address, where files are stored.
                'path' => $_ENV['storagePath'] . 'images', // Or absolute path to directory where files are stored.
                'type' => GetAction::TYPE_IMAGES,
            ],

            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => $_ENV['storageUrl'] . 'images/', // Directory URL address, where files are stored.
                'path' => $_ENV['storagePath'] . 'images' // Or absolute path to directory where files are stored.
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $project = Project::find()->orderBy(['rating' => SORT_DESC])->one();
        Article::attachArticleTextAndDescriptionToProject($project);
        return $this->render('index', [
            'project' => $project,
        ]);
    }





    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
