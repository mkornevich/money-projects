<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/22/2017
 * Time: 12:20 PM
 */

namespace app\controllers;

use app\components\project\ProjectFinder;
use app\models\base\Article;
use app\models\base\Project;
use app\models\frontend\FrontArticle;
use app\components\sort\ArticleSortManager;
use app\models\frontend\FrontProject;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ArticleController extends Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => AccessControl::className(),
                'only' => ['edit', 'new', 'remove'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['edit', 'new', 'remove'],
                        'roles' => ['editAndAddArticle'],
                    ]
                ],

            ],
        ];
    }

    public function actionView($id)
    {
        $query = Article::find()->where(['id' => $id]);

        if (!Yii::$app->user->can('editAndAddArticle'))
            $query->andWhere('created_at < ' . time())
                ->andWhere('draft = 0');

        if (!$article = $query->one())
            throw new NotFoundHttpException('Статья не найдена');

        return $this->render('view', [
            'article' => $article,
            'canEditAndAddArticle' => Yii::$app->user->can('editAndAddArticle'),
        ]);
    }

    public function actionList()
    {

        $pagination = new Pagination();
        $articleSortManager = new ArticleSortManager(Yii::$app->request->get());

        $articlesQuery = $articleSortManager->getPrepareQuery(Article::find());

        if (!Yii::$app->user->can('editAndAddArticle'))
            $articlesQuery->andWhere('created_at < ' . time())
                ->andWhere('draft = 0');

        // для показа всех статей для проекта
        if ($projectId = Yii::$app->request->get('for-project')) {
            $articlesQuery->andWhere(['target_id' => (int)$projectId]);
            $articlesQuery->andWhere(['target_type' => Article::TARGET_TYPE_PROJECT]);
        }

        $pagination->totalCount = $articlesQuery->count();
        $pagination->defaultPageSize = 5;

        $articles = $articlesQuery
            ->limit($pagination->limit)
            ->offset($pagination->offset)
            ->all();

        return $this->render('list', [
            'pagination' => $pagination,
            'articles' => $articles,
            'columnSortViewData' => $articleSortManager->getPrepareViewData(),
            'canEditAndAddArticle' => Yii::$app->user->can('editAndAddArticle'),
        ]);
    }

    public function actionEdit($id)
    {
        $articleEditForm = FrontArticle::findOne($id);

        if (!$articleEditForm)
            throw new BadRequestHttpException('Такой статьи не существует.');

        $articleEditForm->scenario = FrontArticle::SCENARIO_EDIT_FORM;

        if (
            $articleEditForm->load(Yii::$app->request->post()) &&
            $articleEditForm->save()
        )
            return $this->redirect(Url::to(['article/view', 'id' => $articleEditForm->id]));

        return $this->render('edit', [
            'articleEditForm' => $articleEditForm,
            'articleRemoveForm' => new FrontArticle(['scenario' => FrontArticle::SCENARIO_REMOVE_FORM]),
        ]);
    }

    public function actionRemove()
    {
        $articleRemoveForm = new FrontArticle(['scenario' => FrontArticle::SCENARIO_REMOVE_FORM]);
        if (
            $articleRemoveForm->load(Yii::$app->request->post()) &&
            $articleRemoveForm->validate() &&
            $articleRemoveForm->deleteByThisId()
        )
            return $this->redirect(Url::to(['article/list']));
        else
            throw new BadRequestHttpException('Не удалось удалить статью.');
    }

    public function actionNew($id = '', $type = '', $mainProjectId = '')
    {
        $articleNewForm = new FrontArticle(['scenario' => FrontArticle::SCENARIO_NEW_FORM]);

        if (
            $articleNewForm->load(Yii::$app->request->post()) &&
            $articleNewForm->save()
        ){

            if(
                $mainProjectId != '' &&
                $project = Project::findOne($mainProjectId)
            ){
                $project->target_article = $articleNewForm->id;
                $project->save();
            }
            return $this->redirect(Url::to(['article/view', 'id' => $articleNewForm->id]));

        }

        $articleNewForm->target_type = $type;
        $articleNewForm->target_id = $id;

        return $this->render('new', [
            'articleNewForm' => $articleNewForm,
        ]);
    }
}