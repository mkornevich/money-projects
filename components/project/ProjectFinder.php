<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 11/19/2017
 * Time: 6:32 PM
 */

namespace app\components\project;


use app\models\base\Article;
use app\models\base\Category;
use app\models\base\Project;
use app\models\base\ProjectCategory;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class ProjectFinder
{
    private $categoryIds = [];


    public function __construct($categoryIds)
    {
        $this->categoryIds = $this->checkCategoryIds($categoryIds);
    }

    private function checkCategoryIds($categoryIds)
    {
        $result = [];

        if (is_array($categoryIds))
            foreach ($categoryIds as $stringId)
                $result[] = (int)$stringId;

        return $result;

    }

    private function queryFindProjectByCategoriesIds($categoryIds, $limit, $offset): Query
    {
        $projectIds = ProjectCategory::find()
            ->select(['project_id'])
            ->where(['category_id' => $categoryIds])
            ->groupBy(['project_id'])
            ->having('COUNT(*) = ' . count($categoryIds));

        return Project::find()
            ->where(['id' => $projectIds])
            ->orderBy(['rating' => SORT_DESC])
            ->limit($limit)
            ->offset($offset);
    }

    // грузит категории к массиву с проектами
    private function attachCategoryDataToProjectArray($projects)
    {
        $resultProjects = $projects;

        $projectIds = ArrayHelper::getColumn($projects, 'id');

        $projectCategories = ProjectCategory::find()
            ->select(['project_id', 'category_id'])
            ->where(['project_id' => $projectIds])
            ->asArray()->all();

        foreach ($resultProjects as &$resultProject) {
            $resultProject['categoryIds'] = [];

            foreach ($projectCategories as $projectCategory) {
                if ($projectCategory['project_id'] == $resultProject['id'])
                    $resultProject['categoryIds'][] = $projectCategory['category_id'] + 0;
            }

        }

        return $resultProjects;
    }


    private function preprocessedCategoriesArray($categoryArray)
    {
        $preprocessedCategoryArray = [];

        foreach ($categoryArray as $categoryItem) {
            $item = [
                'name' => $categoryItem['name'],
                'rating' => $categoryItem['rating'],
                'foundation_year' => $categoryItem['foundation_year'],
                'id' => $categoryItem['id'],
                'target_article' => $categoryItem['target_article'],
                'url_to_project' => $categoryItem['url'],
                'url_to_edit' => Url::to(['project/edit', 'id' => $categoryItem['id']]),
                'url_to_view' => Url::to(['project/view', 'id' => $categoryItem['id']])
            ];
            $preprocessedCategoryArray[] = $item;
        }

        $preprocessedCategoryArray = Article::attachDescriptionToProjectsArray($preprocessedCategoryArray);
        $preprocessedCategoryArray = $this->attachCategoryDataToProjectArray($preprocessedCategoryArray);

        return $preprocessedCategoryArray;
    }

    private function getPreprocessedProjectsByCategoryIds($limit, $offset)
    {

        if (count($this->categoryIds) > 0)
            $result = $this->queryFindProjectByCategoriesIds($this->categoryIds, $limit, $offset)->asArray()->all();
        else
            $result = Project::find()->orderBy(['rating' => SORT_DESC])->limit($limit)->offset($offset)->asArray()->all();


        return $this->preprocessedCategoriesArray($result);
    }

    // export

    public function getProjectsByCategoryIds($page)
    {
        return $this->getPreprocessedProjectsByCategoryIds(7, 7 * $page);
    }

    public function getStartProjects()
    {

        $result = Project::find()->orderBy(['rating' => SORT_DESC])->asArray()->limit(7)->all();
        $result = $this->preprocessedCategoriesArray($result);
        return $result;
    }
}