<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 11/18/2017
 * Time: 6:32 PM
 */

namespace app\components\category;


use app\models\base\Category;
use Yii;

class CategoryFinder
{
    private $categories;

    private function loadCategoriesAndSetDefault(){
        $categories = (new CategoryData())->getCategoryData();

        foreach ($categories as &$category){
            $category['parentId'] = $category['parentId'] ?? 0;
            $category['checkBoxVisible'] = $category['checkBoxVisible'] ?? true;
            $category['expand'] = $category['expand'] ?? false;
            $category['select'] = $category['select'] ?? false;
            $category['childrenMultiSelect'] = $category['childrenMultiSelect'] ?? true;
        }

        $this->categories = $categories;
    }

    public function __construct()
    {
        $this->loadCategoriesAndSetDefault();
    }

    public function getAllArrayCategories(){
        return $this->categories;
    }

}