<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 11/29/2017
 * Time: 9:26 PM
 */

namespace app\components\category;


class CategoryData
{
    public function getCategoryData()
    {
        /**
         * id - идентификатор category/get-new-id
         * name - имя
         * checkBoxVisible : true - показывать ли чекбокс
         * childrenMultiSelect : true - у детей будут чекбоксы или комбобоксы
         * parentId : 0 - родительский идентификатор
         * expand : false - развернут ли пункт
         * select : false - чекнут ли чекбокс
         */
        return [

            // тип платежной системы

            [
                'id' => 1,
                'name' => 'Тип платежной системы',
                'checkBoxVisible' => false
            ],

            [
                'id' => 3,
                'name' => 'webmoney.ru',
                'parentId' => 1,
            ],

            [
                'id' => 4,
                'name' => 'money.yandex.ru',
                'parentId' => 1,
            ],

            [
                'id' => 5,
                'name' => 'qiwi.com',
                'parentId' => 1,
            ],

            [
                'id' => 6,
                'name' => 'биткоин',
                'parentId' => 1,
            ],

            [
                'id' => 14,
                'name' => 'perfectmoney.is',
                'parentId' => 1,
            ],

            [
                'id' => 15,
                'name' => 'payeer.com',
                'parentId' => 1,
            ],

            [
                'id' => 16,
                'name' => 'payza.com',
                'parentId' => 1,
            ],

            // Тип валюты

            [
                'id' => 10,
                'name' => 'Валюта',
                'checkBoxVisible' => false,
                'childrenMultiSelect' => false,
            ],

            [
                'id' => 11,
                'parentId' => 10,
                'name' => 'USD доллар'
            ],
            [
                'id' => 12,
                'parentId' => 10,
                'name' => 'RUB рубль'
            ],
            [
                'id' => 13,
                'parentId' => 10,
                'name' => 'EUR евро'
            ],



            // заработок на

            [
                'id' => 2,
                'name' => 'Заработок на:',
                'checkBoxVisible' => false,
                'childrenMultiSelect' => false,
                'expand' => true,
            ],

            [
                'id' => 7,
                'name' => 'Буксах',
                'parentId' => 2,
            ],

            [
                'id' => 8,
                'name' => 'Биткоин кранах',
                'parentId' => 2,
            ],

            [
                'id' => 9,
                'name' => 'Мобильном телефоне',
                'parentId' => 2,
            ],

            [
                'id' => 22,
                'parentId' => 2,
                'name' => 'Вводе капчи',
            ],

            [
                'id' => 23,
                'parentId' => 2,
                'name' => 'Написании статей',
            ],

            [
                'id' => 24,
                'parentId' => 2,
                'name' => 'Сайте',
            ],

            [
                'id' => 25,
                'parentId' => 2,
                'name' => 'Ссылках',
            ],

            [
                'id' => 26,
                'parentId' => 2,
                'name' => 'Файлообменниках',
            ],

            [
                'id' => 27,
                'parentId' => 2,
                'name' => 'Ставках на спорт',
            ],

            [
                'id' => 28,
                'parentId' => 2,
                'name' => 'Ставках на курс валют',
            ],

            [
                'id' => 29,
                'parentId' => 2,
                'name' => 'Плагинах',
            ],

            [
                'id' => 30,
                'parentId' => 2,
                'name' => 'Компютерных программах',
            ],

            [
                'id' => 31,
                'parentId' => 2,
                'name' => 'Партнерской программе',
            ],
        ];
    }
}