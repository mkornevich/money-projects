<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 11/7/2017
 * Time: 1:15 PM
 */

namespace app\components\sort;

use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

abstract class AbstractSortManager
{

    private $requestGetData;

    public function __construct($requestGetData)
    {
        $this->requestGetData = $requestGetData;
    }

    abstract protected function config();

    public function getPrepareQuery(Query $query)
    {
        $resultQuery = $query;

        foreach ($this->config() as $sortGroup) {

            $thisGetParam = $this->requestGetData[$sortGroup['name']] ?? $sortGroup['default'];

            foreach ($sortGroup['items'] as $sortGroupItem) {

                if ($sortGroupItem['value'] == $thisGetParam) {

                    $resultQuery = $sortGroupItem['function']($resultQuery);

                    break;
                }

            }
        }

        return $resultQuery;
    }

    public function getPrepareViewData()
    {
        $resultViewData = $this->config();
        foreach ($resultViewData as &$sortGroup) {

            $thisGetParam = $this->requestGetData[$sortGroup['name']] ?? $sortGroup['default'];

            foreach ($sortGroup['items'] as $index => &$sortGroupItem) {

                if ($sortGroupItem['value'] == $thisGetParam) {
                    $sortGroupItem['isActive'] = true;
                    $sortGroupItem['goUrl'] = false;


                } else {
                    $sortGroupItem['isActive'] = false;
                    $sortGroupItem['goUrl'] = $this->changeGetDataAndGenerateUrl($sortGroup['name'], $sortGroupItem['value']);
                }
            }
        }

        return $resultViewData;
    }

    private function changeGetDataAndGenerateUrl($key, $value)
    {
        $getParamsArray = $this->requestGetData;
        $getParamsArray[$key] = $value;

        $toUrlGenerate = ArrayHelper::merge(['article/list'], $getParamsArray);

        return Url::to($toUrlGenerate);
    }
}