<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 11/9/2017
 * Time: 9:35 PM
 */

namespace app\components\sort;

use app\models\base\Article;
use Yii;
use yii\db\Query;

class ArticleSortManager extends AbstractSortManager
{
    protected function config()
    {
        $result[] = [
            'name' => 'date',
            'text' => 'По дате публикации',
            'default' => 0,
            'items' => [
                [
                    'value' => 0,
                    'text' => 'Сначало новые',
                    'function' => function (Query $query) {
                        return $query->orderBy(['created_at' => SORT_DESC]);
                    }
                ],
                [
                    'value' => 1,
                    'text' => 'Сначало старые',
                    'function' => function (Query $query) {
                        return $query->orderBy(['created_at' => SORT_ASC]);
                    }
                ]
            ]
        ];

        $result[] = [
            'name' => 'type',
            'text' => 'По типу статей',
            'default' => 0,
            'items' => [
                [
                    'value' => 0,
                    'text' => 'Все статьи',
                    'function' => function (Query $query) {
                        return $query;
                    }
                ],
                [
                    'value' => 1,
                    'text' => 'Статьи для категорий',
                    'function' => function (Query $query) {
                        return $query->andWhere(['target_type' => Article::TARGET_TYPE_CATEGORY]);
                    }
                ],
                [
                    'value' => 2,
                    'text' => 'Статьи для проектов',
                    'function' => function (Query $query) {
                        return $query->andWhere(['target_type' => Article::TARGET_TYPE_PROJECT]);
                    }
                ],
                [
                    'value' => 3,
                    'text' => 'Простые статьи',
                    'function' => function (Query $query) {
                        return $query->andWhere('target_id = 0');
                    }
                ]
            ]
        ];

        if (Yii::$app->user->can('editAndAddArticle'))

            $result[] = [
                'name' => 'admin',
                'text' => 'Для админа',
                'default' => 0,
                'items' => [
                    [
                        'value' => 0,
                        'text' => 'Все статьи',
                        'function' => function (Query $query) {
                            return $query;
                        }
                    ],
                    [
                        'value' => 1,
                        'text' => 'Запланированные статьи',
                        'function' => function (Query $query) {
                            return $query->andWhere('created_at >' . time());
                        }
                    ],
                    [
                        'value' => 2,
                        'text' => 'Черновики',
                        'function' => function (Query $query) {
                            return $query->andWhere('draft = 1');
                        }
                    ]
                ]
            ];


        return $result;
    }
}