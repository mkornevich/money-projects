<?php
return [
    'editAndAddArticle' => [
        'type' => 2,
        'description' => 'Редактирование и добавление статей',
    ],

    'editAndAddProject' => [
        'type' => 2,
        'description' => 'Редактирование и добавление проектов',
    ],

    'editAndAddCategory' => [
        'type' => 2,
        'description' => 'Редактирование и добавление категорий',
    ],

    'loadImages' => [
        'type' => 2,
        'description' => 'Загружать изображения',
    ],

    'admin' => [
        'type' => 1,
        'children' => [
            'editAndAddProject',
            'loadImages',
            'editAndAddCategory',
            'editAndAddArticle'
        ],
    ],
];
