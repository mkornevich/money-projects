<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 11/19/2017
 * Time: 2:58 PM
 */

namespace app\commands;


use app\components\category\CategoryData;
use Yii;
use yii\console\Controller;

class CategoryController extends Controller
{

    /**
     * method for testing
     */
    public function actionTest(){
        echo 'fffff';
    }

    /**
     * method for testing
     */
    public function actionGetNewId(){
        $newId = 1;
        $nextFind = true;

        $categories = (new CategoryData())->getCategoryData();



        while ($nextFind){
            foreach ($categories as $key => $category){
                if($category['id'] == $newId){
                    $newId++;
                    break;
                }elseif (count($categories) == ($key + 1)){
                    $nextFind = false;
                }
            }
        }

        echo 'New id = ' . $newId;
    }
}