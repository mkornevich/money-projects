<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/10/2017
 * Time: 9:23 AM
 */

namespace app\commands;

use app\models\User;
use Yii;
use yii\console\Controller;

class TestController extends Controller
{
    public function actionAddAdmin()
    {

        $user = new User();
        $user->username = 'mkornevich3';
        $user->email = 'mkornevich3@gmail.com';
        $user->setPassword('otipaz33');
        $user->generateAuthKey();
        if ($user->save()) {
            echo 'good';
        }

    }

    public function actionAddPermission()
    {
        $auth = Yii::$app->authManager;

        // добавляем разрешение "createPost"
        $createPost = $auth->createPermission('createPost');
        $createPost->description = 'Create a post';
        $auth->add($createPost);

        // добавляем разрешение "updatePost"
        $updatePost = $auth->createPermission('updatePost');
        $updatePost->description = 'Update post';
        $auth->add($updatePost);

        // добавляем роль "author" и даём роли разрешение "createPost"
        $author = $auth->createRole('author');
        $auth->add($author);
        $auth->addChild($author, $createPost);

        // добавляем роль "admin" и даём роли разрешение "updatePost"
        // а также все разрешения роли "author"
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $updatePost);
        $auth->addChild($admin, $author);

        // Назначение ролей пользователям. 1 и 2 это IDs возвращаемые IdentityInterface::getId()
        // обычно реализуемый в модели User.
        $auth->assign($author, 2);
        $auth->assign($admin, 1);
    }
}