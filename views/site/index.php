<?php

/**
 * @var $this yii\web\View
 * @var $project \app\models\base\Project
 **/

use yii\helpers\Url;

$this->title = 'Начать зарабатывать';
?>

<div class="content-panel" id="index-page">


    <div class="row">
        <div class="col-md-12">
            <div class="page-index-container">
                <h3>Приветствуем вас!</h3>
                <p>
                    <span class="green font-bold">Money</span> <span class="orange font-bold">Catalog</span> - сервис который поможет найти вам удобный для вас способ
                    заработка денег в интернете. Скажу сразу для того чтобы добится хорошего дохода
                    ничего не делая нужно невероятно сильно постаратся изначально, за
                    бесценок, набираясь опыта и потом потихоньку прибыль будет становися все
                    больше и больше. И запомните без опыта много денег не заработать.
                    Нужно постепенно узнавать о разных способах заработка, искать какой будет лучше
                    и на них не ленясь зарабатывать.
                    <a class="btn btn-default pull-right" href="<?= Url::to(['project/list']) ?>">Проекты для заработка</a>
                </p>

            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="page-index-container">
                <h3>Возможности:</h3>
                <ol>
                    <li>Удобная сортировка и поиск проектов.</li>
                    <li>Имеется описание на большое количество проектов.</li>
                    <li>Отображается много полезной информации о проекте.</li>
                    <li>Про некоторые проекты написано нескольео статей.</li>
                    <li>Имеется система предложенных статей.</li>
                </ol>
            </div>
        </div>

        <div class="col-md-6">
            <div class="page-index-container fix-overflow">
                <h3>Попробуйте проект <b><?= $project->name ?></b></h3>
                <p><?= $project->description ?></p>

                <a class="btn btn-default pull-right" href="<?= Url::to(['project/view', 'id' => $project->id]); ?>">Подробнее...</a>
            </div>
        </div>
    </div>




</div>
