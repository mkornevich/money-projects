<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="content-panel">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        Произошла ошибка при обработке вашего запроса.
    </p>
    <p>
        Пожалуйста если вы считаете что эта ошибка серьезная сообщите нам. Спасибо.
    </p>

</div>
