<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/11/2017
 * Time: 11:03 AM
 *
 * @var \app\models\frontend\FrontProject $projectEditForm
 * @var \app\models\frontend\FrontProject $projectRemoveForm
 * @var array $categories
 * @var integer $projectId
 */


use app\widgets\AttachedCategories\AttachedCategories;
use dosamigos\datepicker\DatePicker;
use vova07\imperavi\Widget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Редактирование проекта';

?>


<div class="content-panel">

    <div class="page-title">
        <span class="item app-color-blue app-header-font">Редактирование проекта <b><?= Html::encode($projectEditForm->name) ?></b></span>
    </div>

    <? $f = ActiveForm::begin() ?>

    <div class="row">

        <div class="col-md-6">

            <div class="row">
                <div class="col-sm-6">
                    <?= $f->field($projectEditForm, 'name')->textInput() ?>
                </div>
                <div class="col-sm-6">
                    <?= $f->field($projectEditForm, 'url')->textInput() ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <?= $f->field($projectEditForm, 'foundation_year')->input('number', ['max' => 9999, 'min' => 0]) ?>
                </div>
                <div class="col-sm-4">
                    <?= $f->field($projectEditForm, 'rating')->input('number') ?>
                </div>
                <div class="col-sm-4">
                    <?= $f->field($projectEditForm, 'target_article')->input('number') ?>
                    <? if( $projectEditForm->target_article ): ?>
                        <a class="btn btn-default pull-right" href="<?= Url::to(['article/edit', 'id' => $projectEditForm->target_article]) ?>">Редактировать статью</a>
                    <? else: ?>




                        <? if ($projectEditForm->target_article > 0): ?>
                            <a target="_blank" class="btn btn-default pull-right" href="<?= Url::to(['article/edit', 'id' => $projectEditForm->target_article]) ?>">Редактировать гл. статью</a>
                        <? else: ?>
                            <a target="_blank" class="btn btn-default pull-right" href="<?= Url::to(['article/new', 'mainProjectId' => $projectEditForm->id]) ?>">Добавить гл. статью</a>
                        <? endif; ?>


                    <? endif; ?>
                </div>
            </div>

            <div style="margin-top: 10px" class="fix-overflow">
                <div class="btn-group pull-right">

                    <?= Html::submitButton('Применить изменения', ['class' => 'btn btn-primary', 'formaction' => Url::to(['project/edit', 'id' => $projectId])]) ?>

                    <?= Html::submitButton('Удалить', ['class' => 'btn btn-danger', 'formaction' => Url::to(['project/remove'])]) ?>


                </div>
            </div>
            <?= $f->field($projectRemoveForm, 'id')->hiddenInput(['value' => $projectId])->label(false) ?>


        </div>

        <div class="col-md-6">
            <?= $f->field($projectEditForm, 'selectCategory')->widget(AttachedCategories::className()) ?>
        </div>



    </div>





    <? ActiveForm::end() ?>


</div>


