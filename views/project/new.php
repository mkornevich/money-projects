<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/11/2017
 * Time: 11:03 AM
 *
 * @var \app\models\frontend\FrontProject $projectNewForm
 * @var array $categories
 */

use app\widgets\AttachedCategories\AttachedCategories;
use dosamigos\datepicker\DatePicker;
use vova07\imperavi\Widget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Добавление проекта';

?>

<div class="content-panel">

    <div class="page-title">
        <span class="item app-color-blue app-header-font">Добавление нового проекта</span>
    </div>

    <? $f = ActiveForm::begin() ?>

    <div class="row">

        <div class="col-md-6">

            <div class="row">
                <div class="col-sm-6">
                    <?= $f->field($projectNewForm, 'name')->textInput() ?>
                </div>
                <div class="col-sm-6">
                    <?= $f->field($projectNewForm, 'url')->textInput() ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <?= $f->field($projectNewForm, 'foundation_year')->input('number', ['max' => 9999, 'min' => 0]) ?>
                </div>
                <div class="col-sm-4">
                    <?= $f->field($projectNewForm, 'rating')->input('number') ?>
                </div>
                <div class="col-sm-4">
                    <?= $f->field($projectNewForm, 'target_article')->input('number') ?>
                </div>
            </div>

            <div style="margin-top: 10px" class="fix-overflow">
                <div class="btn-group pull-right">

                    <?= Html::submitButton('Добавить проект', ['class' => 'btn btn-primary']) ?>

                </div>
            </div>


        </div>

        <div class="col-md-6">
            <?= $f->field($projectNewForm, 'selectCategory')->widget(AttachedCategories::className()) ?>
        </div>



    </div>

    <? ActiveForm::end() ?>

</div>