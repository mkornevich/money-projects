<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 11/9/2017
 * Time: 9:50 PM
 *
 * @var yii/web/View $this
 * @var array $jsonCategories
 */


use app\assets\ProjectListAsset;
use app\assets\VueAsset;
use app\components\category\CategoryFinder;
use yii\helpers\Url;

VueAsset::register($this);
ProjectListAsset::register($this);

$this->title = 'Проекты';

?>


<div class="site-list" id="project-list-viewer" hidden>

    <div v-if="alertLoadedBlockShow" class="alert alert-success">
        Подождите пожалуйста мы загружаем данные!
    </div>


    <div class="item" v-for="project in projects">

        <div class="col-one">
            <div class="p-panel">
                <div class="p-panel-header red"><i class="glyphicon glyphicon-folder-close"></i>
                    {{ project.name }}
                </div>
                <div class="p-panel-body">
                    <p v-html="project.description || 'К этому проекту краткое описание не задано.' "></p>
                </div>
            </div>

            <div class="p-panel">
                <div class="p-panel-header grey"><i class="glyphicon glyphicon-tags"></i>Категории</div>
                <div class="p-panel-body">
                    <div class="category-viewer">
                        <div class="category-group" v-for="item in parseCategory(project.categoryIds)">
                            <span class="category-group-name">{{ item.rootName }} </span>
                            <span class="category-item" v-for="childName in item.names">{{ childName }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-two">
            <div class="p-panel">
                <div class="p-panel-header green"><i class="glyphicon glyphicon-info-sign"></i>Информация
                </div>
                <div class="p-panel-body">
                    <div class="info-viewer">
                        <div class="info-group">
                            <i class="glyphicon glyphicon-star red"></i>
                            <span class="info-name">Рейтинг</span>
                            <span class="info-value">{{ project.rating }}</span>
                        </div>
                        <div class="info-group">
                            <i class="glyphicon glyphicon-time green"></i>
                            <span class="info-name">Год основания</span>
                            <span class="info-value">{{ (project.foundation_year == 0) ? 'не указано' : project.foundation_year}}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="p-panel">
                <div class="p-panel-header grey"><i class="glyphicon glyphicon-th-list"></i>Действия</div>
                <div class="p-panel-body">
                    <div class="actions">
                        <ul>
                            <li v-if="canEditAndAddProject">
                                <a target="_blank"
                                   :href="project.url_to_edit"
                                   class="button view">
                                    Редактировать
                                </a>
                            <li>
                                <a target="_blank"
                                   :href="project.url_to_view"
                                   class="button view">
                                    Читать подробнее
                                </a>
                            <li>
                                <a target="_blank"
                                   :href="project.url_to_project"
                                   class="button show-site">
                                    Открыть сайт
                                </a>

                        </ul>
                    </div>
                </div>
            </div>


        </div>

    </div>


    <div v-if="projects.length < 1" class="alert alert-danger">
        Ничего не найдено!
    </div>

    <button v-else @click="loadNext" id="load-next-button" class="btn btn-primary">Загрузить еще</button>

</div>


<? $this->beginBlock('leftColumn') ?>

<div class="site-column-style" hidden>
    <div class="title">Панель поиска</div>

    <div class="sub-title">Поиск по названию</div>

    <div id="search" class="content">
        <div class="search-element">
            <input @keyup="loadSearchData" placeholder="Начинайте вводить" v-model="searchQuery" class="search-input" type="text">
            <i @click="searchQuery = ''" v-if="searchQuery != ''" class="remove glyphicon glyphicon-remove red"></i>
        </div>

        <div v-if="searchQuery !== ''" class="search-result">
            <div v-for="item in searchResult" @click="goToProject(item.id)" class="search-item">{{item.name}}</div>
        </div>

    </div>

    <div class="sub-title">Поиск по категориям</div>

    <div id="category-tree" class="content-no-padding">
        <div v-if="parentCategory.parentId === 0" v-for="parentCategory in categoryData">
            <div @click="parentCategory.expand = !parentCategory.expand" class="parent-category">
                <i :class="'glyphicon glyphicon-triangle-' + (parentCategory.expand ? 'top': 'bottom') + ' orange'"></i> {{ parentCategory.name }}
            </div>
            <div v-if="parentCategory.expand" class="children-list">
                <div @click="toggleCheckBox(childrenCategory)" class="children-category" v-for="childrenCategory in getChildren(parentCategory)">
                    <span :style="childrenCategory.select ? 'color: black;' : 'color: grey;'">{{childrenCategory.name}}</span>
                </div>
            </div>

        </div>

    </div>


</div>


<? $this->endBlock() ?>


<script type="text/javascript">

    phpProvider = JSON.parse('<?= $phpProvider ?>');

</script>
