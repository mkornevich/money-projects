<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/7/2017
 * Time: 9:47 PM
 *
 * @var \app\models\base\Project $project
 * @var array $categories
 * @var view $this ;
 *
 * @var bool $canEditAndAddArticle ;
 * @var bool $canEditAndAddProject ;
 * @var bool $articleToThisProjectExist
 */

use app\models\frontend\FrontArticle;
use app\widgets\SuggestedArticles\SuggestedArticles;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = 'Проект - ' . Html::encode($project->name);

?>


<div class="content-panel">


    <div class="page-title">
        <span class="font-size-2 blue">Проект <b><?= Html::encode($project->name) ?></b></span>
    </div>


    <div class="fix-overflow">
        <p><?= $project->text ?? 'Сейчас здесь нет текста загляните позже' ?></p>
    </div>


    <!-- comments -->
    <div id="disqus_thread"></div>
    <script>

        var disqus_config = function () {
            this.page.url = '<?= Url::to(['project/view', 'id' => $project->id], true) ?>';  // Replace PAGE_URL with your page's canonical URL variable
            this.page.identifier = '<?= 'project_' . $project->id ?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };

        (function () { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            s.src = 'https://money-catalog.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by
            Disqus.</a></noscript>

</div>












<? $this->beginBlock('leftColumn'); ?>

<div class="site-column-style">
    <? if ($canEditAndAddArticle or $canEditAndAddProject or $articleToThisProjectExist): ?>
        <div class="title">Действия</div>
    <? endif; ?>

    <div class="content-no-padding">

        <? if ($canEditAndAddProject): ?>
            <a class="styled-item" href="<?= Url::to(['project/edit', 'id' => $project->id]) ?>">
                <i class="glyphicon glyphicon-pencil"></i>
                <span class="orange">Редактировать проект</span>
            </a>
        <? endif; ?>

        <? if ($canEditAndAddArticle): ?>

            <? if ($project->target_article > 0): ?>
                <a class="styled-item"
                   href="<?= Url::to(['article/edit', 'id' => $project->target_article]) ?>">
                    <i class="glyphicon glyphicon-pencil"></i>
                    <span class="green">Редактировать гл. статью</span>
                </a>
            <? else: ?>
                <a class="styled-item"
                   href="<?= Url::to(['article/new', 'mainProjectId' => $project->id]) ?>">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span class="green">Создать гл. статью</span>
                </a>
            <? endif; ?>

            <a class="styled-item"
               href="<?= Url::to(['article/new', 'id' => $project->id, 'type' => FrontArticle::TARGET_TYPE_PROJECT]) ?>">
                <i class="glyphicon glyphicon-list-alt"></i>
                <span class="green">Добавить статью</span>
            </a>
        <? endif; ?>

        <? if ($articleToThisProjectExist): ?>
            <a class="styled-item" href="<?= Url::to(['article/list', 'for-project' => $project->id]) ?>">
                <i class="glyphicon glyphicon-list-alt"></i>
                <span class="blue">См. статьи про этот проект</span>
            </a>
        <? endif; ?>

    </div>
    <div class="title">Информация</div>
    <div class="content-no-padding key-value-styled-info">
        <div class="info-group">
            <i class="glyphicon glyphicon-time blue"></i>
            <span class="info-name">Дата основания</span>

            <? if ($project->foundation_year): ?>
                <span class="info-value"><?= $project->foundation_year ?> г.</span>
            <? else: ?>
                <span class="info-value red">не указана</span>
            <? endif; ?>


        </div>
        <div class="info-group">
            <i class="glyphicon glyphicon-star red"></i>
            <span class="info-name">Рейтинг</span>
            <span class="info-value"><?= Html::encode($project->rating) ?></span>
        </div>
    </div>
    <div class="sub-title">Категории</div>
    <div class="content-no-padding" id="project-view-category-viewer">

        <? foreach ($project->categoryData as $categoryGroup): ?>
            <div class="category-group">
                <div class="category-title"><?= $categoryGroup['rootName'] ?></div>
                <? foreach ($categoryGroup['names'] as $name): ?>
                    <div class="category-item"><?= $name ?></div>
                <? endforeach; ?>
            </div>
        <? endforeach; ?>

    </div>

    <?= SuggestedArticles::widget(['data' => $project]) ?>

</div>

<? $this->endBlock(); ?>




