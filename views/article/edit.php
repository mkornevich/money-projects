<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/22/2017
 * Time: 12:29 PM
 *
 * @var \app\models\frontend\FrontArticle $articleEditForm ;
 * @var \app\models\frontend\FrontArticle $articleRemoveForm ;
 */
use app\models\frontend\FrontArticle;
use dosamigos\datepicker\DatePicker;
use vova07\imperavi\Widget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Редактирование статьи';

?>

<div class="content-panel">

    <div class="page-title">
        <span class="item app-color-blue app-header-font">Редактирование статьи <b><?= Html::encode($articleEditForm->name) ?></b></span>
    </div>

    <? $f = ActiveForm::begin() ?>

    <div class="row">
        <div class="col-sm-3">
            <?= $f->field($articleEditForm, 'name') ?>
        </div>
        <div class="col-sm-3">
            <?= $f->field($articleEditForm, 'formattedCreatedAt')->widget(DatePicker::className(), [
                // modify template for custom rendering
                'language' => 'ru',
                'clientOptions' => [
                    'format' => 'dd.mm.yyyy',
                ]
            ]) ?>
        </div>
        <div class="col-sm-3">
            <?= $f->field($articleEditForm, 'target_id') ?>
        </div>
        <div class="col-sm-3">
            <?= $f->field($articleEditForm, 'target_type')->dropDownList([
                FrontArticle::TARGET_TYPE_CATEGORY => 'Категория',
                FrontArticle::TARGET_TYPE_PROJECT => 'Проект',
            ]) ?>
        </div>
    </div>

    <?= $f->field($articleEditForm, 'description')->textarea() ?>
    <div class="fix-overflow">
        <div class="text-align-right">
            <div class="btn-group">
                <?= Html::submitButton('Изменить', ['class' => 'btn btn-primary', 'formaction' => Url::to(['article/edit', 'id' => $articleEditForm->id])]) ?>
                <?= Html::submitButton('Удалить', ['class' => 'btn btn-danger', 'formaction' => Url::to(['article/remove'])]) ?>
            </div>
            <?= $f->field($articleEditForm, 'draft')->checkbox() ?>
        </div>
    </div>
    <?= $f->field($articleEditForm, 'text')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => Yii::$app->params['imperaviEditorPlugins'],
            'imageManagerJson' => Url::toRoute(['site/images-get']),
            'imageUpload' => Url::toRoute(['site/image-upload']),
        ]
    ]) ?>

    <?= $f->field($articleRemoveForm, 'id')->hiddenInput(['value' => $articleEditForm->id])->label(false) ?>

    <? ActiveForm::end() ?>

</div>