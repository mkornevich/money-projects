<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/22/2017
 * Time: 12:28 PM
 *
 * @var \app\models\base\Article $article ;
 * @var bool $canEditAndAddArticle
 */
use app\widgets\SuggestedArticles\SuggestedArticles;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Статья - ' . Html::encode($article->name);

?>

<div class="content-panel">


    <div class="page-title">
        <span class="font-size-2 blue">Статья <b><?= Html::encode($article->name) ?></b></span>
    </div>


    <div style="overflow: hidden">
        <p><?= $article->text ?></p>
    </div>

<!-- comments -->
    <div id="disqus_thread"></div>
    <script>

        var disqus_config = function () {
            this.page.url = '<?= Url::to(['article/view', 'id' => $article->id], true) ?>';  // Replace PAGE_URL with your page's canonical URL variable
            this.page.identifier = '<?= 'article_' . $article->id ?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };

        (function() { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            s.src = 'https://money-catalog.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>


</div>



<? $this->beginBlock('leftColumn'); ?>

<div class="site-column-style">
    <? if($canEditAndAddArticle): ?>
        <div class="title">Действия</div>
    <? endif; ?>

    <div class="content-no-padding">

        <? if ($canEditAndAddArticle): ?>
            <a class="styled-item" href="<?= Url::to(['article/edit', 'id' => $article->id]) ?>">
                <i class="glyphicon glyphicon-pencil"></i>
                <span class="orange">Редактировать</span>
            </a>
        <? endif; ?>

    </div>
    <div class="title">Информация</div>
    <div style="padding-bottom: 3px" class="content-no-padding key-value-styled-info">

        <? if($article->draft): ?>
            <div class="info-group">
                <i class="glyphicon glyphicon-pencil"></i>
                <span class="info-name">Это черновик</span>
            </div>
        <? endif; ?>

        <div class="info-group">
            <i class="glyphicon glyphicon-time orange"></i>

            <? if($article->created_at and $article->updated_at): ?>
                <? $articleTime = Html::encode(Yii::$app->formatter->asDate($article->created_at, 'php:d.m.yy')) ?>

                <? if ($article->created_at + 86400 > $article->updated_at): ?>
                    <span class="info-name">Cтатья создана</span>
                    <span class="info-value"><?= $articleTime ?></span>
                <? else: ?>
                    <span class="info-name">Cтатья обновлена</span>
                    <span class="info-value"><?= $articleTime ?></span>
                <? endif; ?>
            <? endif; ?>

        </div>

    </div>

    <?= SuggestedArticles::widget(['data' => $article]) ?>

</div>

<? $this->endBlock('leftColumn'); ?>
