<?php
/**
 * @var app\models\base\Article[] $articles
 *
 * @var \yii\data\Pagination $pagination
 *
 * @var \yii\web\View $this
 *
 * @var bool $canEditAndAddArticle
 * @var string[] $targetNames
 *
 * @var array $columnSortViewData
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Статьи';

?>


<div id="site-list" class="site-list">


    <? $if = 0;
    foreach ($articles as $article): ?>
        <div class="item">

            <div class="col-one">
                <div class="p-panel">
                    <div class="p-panel-header red"><i class="glyphicon glyphicon-list-alt"></i>
                        <?= Html::encode($article->name) ?>
                    </div>
                    <div class="p-panel-body">
                        <p>
                            <?= Html::encode($article->description) ?>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-two">
                <div class="p-panel">
                    <div class="p-panel-header green"><i class="glyphicon glyphicon-info-sign"></i>Информация
                    </div>
                    <div class="p-panel-body">
                        <div class="info-viewer">
                            <div class="info-group">
                                <i class="glyphicon glyphicon-time green"></i>
                                <? if ($article->created_at): ?>

                                    <span class="info-name">Опубликована</span>
                                    <span class="info-value">
                                        <?= Yii::$app->formatter->asDate($article->created_at, 'php:d.m.yy') ?>
                                    </span>

                                <? endif; ?>


                            </div>

                            <? if ($article->draft): ?>
                                <div class="info-group">
                                    <i class="glyphicon glyphicon-pencil red"></i>
                                    <span class="info-name">Черновик</span>
                                </div>
                            <? endif; ?>

                        </div>
                    </div>
                </div>

                <div class="p-panel">
                    <div class="p-panel-header grey"><i class="glyphicon glyphicon-th-list"></i>Действия</div>
                    <div class="p-panel-body">
                        <div class="actions">
                            <ul>

                                <? if ($canEditAndAddArticle): ?>
                                <li>
                                    <a href="<?= Url::to(['article/edit', 'id' => $article->id]) ?>"
                                       class="button view">
                                        Редактировать
                                    </a>
                                    <? endif; ?>


                                <li>
                                    <a href="<?= Url::to(['article/view', 'id' => $article->id]) ?>"
                                       class="button view">
                                        Читать полностью
                                    </a>


                            </ul>
                        </div>
                    </div>
                </div>


            </div>

        </div>


        <? $if = 1;
    endforeach;
    if ($if): ?>

        <?= \yii\widgets\LinkPager::widget(['pagination' => $pagination]) ?>

    <? else: ?>
        <div class="alert alert-danger">Ничего не найдено</div>
    <? endif; ?>
</div>

<? $this->beginBlock('leftColumn') ?>
<div class="site-column-style">

    <div class="title">Сортировка</div>


    <? foreach ($columnSortViewData as $sortGroup): ?>

    <div class="sub-title"><?= $sortGroup['text'] ?></div>

    <div class="content-no-padding">
        <? foreach ($sortGroup['items'] as $sortGroupItem): ?>


    <? if ($sortGroupItem['goUrl']): ?>
        <div onclick="location.href = '<?= $sortGroupItem['goUrl'] ?>'" class="styled-item">

            <? else: ?>
            <div class="styled-item">
                <? endif; ?>


                <span class="name <?= ($sortGroupItem['isActive']) ? 'red' : '' ?>"><?= $sortGroupItem['text'] ?></span>

            </div>
            <? endforeach; ?>
        </div>
        <? endforeach; ?>


    </div>
    <? $this->endBlock(); ?>

