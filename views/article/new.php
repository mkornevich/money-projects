<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/22/2017
 * Time: 12:29 PM
 *
 * @var \app\models\frontend\FrontArticle $articleNewForm;
 */
use app\models\frontend\FrontArticle;
use dosamigos\datepicker\DatePicker;
use vova07\imperavi\Widget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Добавление статьи';

?>

<div class="content-panel">

    <div class="page-title">
        <span class="item app-color-blue app-header-font">Добавление новой статьи</span>
    </div>
    <? $f = ActiveForm::begin() ?>

    <div class="row">
        <div class="col-sm-3">
            <?= $f->field($articleNewForm, 'name') ?>
        </div>

        <div class="col-sm-3">
            <?= $f->field($articleNewForm, 'formattedCreatedAt')->widget(DatePicker::className(), [
                'language' => 'ru',
                'clientOptions' => [
                    'format' => 'dd.mm.yyyy',
                ]
            ]) ?>
        </div>

        <div class="col-sm-3">
            <?= $f->field($articleNewForm, 'target_id') ?>
        </div>
        <div class="col-sm-3">
            <?= $f->field($articleNewForm, 'target_type')->dropDownList([
                FrontArticle::TARGET_TYPE_CATEGORY => 'Категория',
                FrontArticle::TARGET_TYPE_PROJECT => 'Проект',
            ]) ?>
        </div>
    </div>

    <?= $f->field($articleNewForm, 'description')->textarea() ?>
    <div class="fix-overflow">
        <div class="pull-right text-align-right">
            <div class="btn-group">
                <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
            </div>
            <?= $f->field($articleNewForm, 'draft')->checkbox() ?>
        </div>
    </div>
    <?= $f->field($articleNewForm, 'text')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => Yii::$app->params['imperaviEditorPlugins'],
            'imageManagerJson' => Url::toRoute(['site/images-get']),
            'imageUpload' => Url::toRoute(['site/image-upload']),
        ]
    ]) ?>

    <? ActiveForm::end() ?>

</div>