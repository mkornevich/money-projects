<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\NavigationDrawerAsset;
use app\widgets\InfoPanel\InfoPanel;
use app\widgets\ManagePanel\ManagePanel;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Menu;
use app\assets\AppAsset;

AppAsset::register($this);
NavigationDrawerAsset::register($this)
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <base href="/">
</head>
<body>
<?php $this->beginBody() ?>


<div class="navigation-drawer">
    <div class="head font-size-3 font-bold text-center">


        <img
                width="30"
                height="30"
                src="img/logo.png"
                alt="logo"><span class="blue">oney</span>
        <span class="orange">Catalog</span>

    </div>

    <div class="title">Меню</div>

    <div id="menu" class="content-no-padding">
        <?= Menu::widget([
            'items' => [
                ['label' => 'Начать зарабатывать', 'url' => ['site/index']],
                ['label' => 'Проекты для заработка', 'url' => ['project/list']],
                ['label' => 'Статьи', 'url' => ['article/list']],
            ],

        ]);
        ?>
    </div>

    <?= ManagePanel::widget() ?>

    <? if ($this->blocks['leftColumn']): ?>
        <?= $this->blocks['leftColumn'] ?>
    <? endif; ?>

    <?= InfoPanel::widget() ?>
</div>


<div id="content">
    <header>

        <div id="menu">


            <? if (!Yii::$app->user->isGuest): ?>


                <? ActiveForm::begin(['action' => Yii::$app->urlManager->createUrl('auth/logout')]) ?>
                <?= Html::submitButton('Выход ' . Html::encode(Yii::$app->user->identity->username), ['class' => 'menu-btn']) ?>
                <? ActiveForm::end() ?>

            <? endif; ?>


        </div>
    </header>

    <section class="yield-content">
        <?= $content ?>
    </section>

</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
