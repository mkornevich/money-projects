<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 11/18/2017
 * Time: 5:57 PM
 */

namespace app\assets;


use yii\web\AssetBundle;

class VueAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/vue';

    /**
     * @inheritdoc
     */
    public $js = [
        'vue.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}