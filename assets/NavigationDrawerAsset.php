<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 1/8/2018
 * Time: 11:14 AM
 */

namespace app\assets;


use yii\web\AssetBundle;

class NavigationDrawerAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/navigationDrawer';


    public $js = [
        'navigationDrawer.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'app\assets\VueAsset',
    ];
}