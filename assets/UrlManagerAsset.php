<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 11/25/2017
 * Time: 6:32 PM
 */

namespace app\assets;


use yii\web\AssetBundle;

class UrlManagerAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/urlManager';

    public $js = [
        'urlManager.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
}