/**
 * Created by mkornevich on 11/25/2017.
 */

var Url = (function () {
    return {
        to: function (router, param) {
            var buildParam = '';
            if(param) {
                buildParam = $.param(param || {}) + '&';
            }
            var buildNameRouter = router.replace(/\//g , '/');

            return '/' + buildNameRouter + '?' + buildParam;
        }
    };
})();