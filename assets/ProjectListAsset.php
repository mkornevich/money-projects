<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 11/18/2017
 * Time: 5:58 PM
 */

namespace app\assets;


use yii\web\AssetBundle;

class ProjectListAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/projectList';


    public $js = [
        'projectList.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'app\assets\VueAsset',
        'app\assets\UrlManagerAsset',
    ];
}