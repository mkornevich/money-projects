/**
 * Created by mkornevich on 11/12/2017.
 */


var main = function (phpProvider) {

    var timerId = 0;

    var siteList = new Vue({
        el: '#project-list-viewer',
        data: {
            alertLoadedBlockShow: false,
            projects: phpProvider.startProjects,
            canEditAndAddProject: phpProvider.canEditAndAddProject
        },

        methods: {
            parseCategory: function (categoryIds) {


                var rootCategories = [];
                var resultData = [];

                phpProvider.startCategories.forEach(function (category) {
                    if (category.parentId === 0) {
                        rootCategories.push(category);
                    }
                });

                rootCategories.forEach(function (rootCategory) {
                    var item = {};
                    item.rootName = rootCategory.name;
                    item.names = [];

                    phpProvider.startCategories.forEach(function (category) {
                        if (category.parentId === rootCategory.id && categoryIds.indexOf(category.id) !== -1) {
                            item.names.push(category.name);
                        }
                    });
                    resultData.push(item);
                });

                //console.log(rootCategories);

                return resultData;
            },
            loadNext: function (event) {
                $.ajax({
                    url: Url.to('project/get-projects-json'),
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        siteList.projects = siteList.projects.concat(data);

                        if (data.length < 7) {
                            $('#load-next-button').hide();
                        }

                        nextPage++;
                    },
                    data: {
                        categories: _selectIds,
                        page: nextPage
                    }
                });
            }
        }
    });

    var search = new Vue({
        el: '#search',
        data: {
            searchQuery: '',
            searchResult: []
        },

        methods: {
            goToProject: function (id) {
                location.href = Url.to('project/view', {id: id});
            },

            loadSearchData: function () {

                clearInterval(timerId);
                var self = this;

                timerId = setTimeout(function () {
                    if(self.searchQuery !== '') {
                        $.ajax({
                            url: Url.to('project/search-project', {query: self.searchQuery}),
                            type: 'GET',
                            dataType: 'json',
                            success: function (data) {
                                self.searchResult = data;
                            }

                        });
                    }
                }, 100);
            }
        }
    });

    var categoryTree = new Vue({
        'el': '#category-tree',
        data: {
            categoryData: phpProvider.startCategories
        },

        methods: {

            getChildren: function (parentCategory) {
                var childrenCategory = [];
                this.categoryData.forEach(function (category) {
                    if(category.parentId === parentCategory.id){
                        childrenCategory.push(category);
                    }
                });
                return childrenCategory;
            },

            toggleCheckBox: function (category) {
                category.select = !category.select;

                var selectIds = [];

                this.categoryData.forEach(function (categoryItem) {
                    if (categoryItem.select === true) {
                        selectIds.push(categoryItem.id);
                    }
                });

                $('#category-tree').trigger('onselect', [selectIds]);

            }
        }
    });


    var _selectIds = [];
    var nextPage = 1; // для того чтобы знать что грузить дальше

    $('#category-tree').bind('onselect', function (event, selectIds) {

        $('#load-next-button').show();
        _selectIds = selectIds;
        nextPage = 1;

        siteList.alertLoadedBlockShow = true;

        clearInterval(timerId);
        timerId = setTimeout(function () {
            $.ajax({
                url: Url.to('project/get-projects-json'),
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    siteList.projects = data;
                    siteList.alertLoadedBlockShow = false;
                },
                data: {categories: selectIds}

            });
        }, 1500);

    });


    $('#project-list-viewer').show();
    $('.site-column-style').show();
};

main(phpProvider);