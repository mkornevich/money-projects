<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/17/2017
 * Time: 8:05 PM
 */

namespace app\models\frontend;


use app\models\base\Article;
use app\models\base\Project;
use app\models\base\ProjectCategory;

class FrontProject extends Project
{
    const SCENARIO_NEW_FORM = 'new_form';
    const SCENARIO_EDIT_FORM = 'edit_form';
    const  SCENARIO_REMOVE_FORM = 'remove_form';


    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[static::SCENARIO_REMOVE_FORM] = ['id'];

        $scenarios[static::SCENARIO_EDIT_FORM] =
            ['name', 'url', 'rating', 'target_article', 'selectCategory', 'foundation_year'];

        $scenarios[static::SCENARIO_NEW_FORM] =
            ['name', 'url', 'rating', 'target_article', 'selectCategory', 'foundation_year'];

        return $scenarios;
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя проекта',
            'url' => 'Ссылка на проект',
            'rating' => 'Рейтинг проекта',
            'foundation_year' => 'Год основания',
            'target_article' => 'id статьи проекта',
            'selectCategory' => 'Категории',
        ];
    }

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer', 'max' => 999999],

            ['name', 'required'],
            ['name', 'string', 'max' => 60],

            ['url', 'default', 'value' => ''],
            ['url', 'string', 'max' => 65],

            ['rating', 'default', 'value' => 1],
            ['rating', 'integer', 'max' => 255],

            ['target_article', 'default', 'value' => 0],
            ['target_article', 'integer', 'max' => 999999],
            ['target_article', 'articleExist'],

            ['selectCategory', 'safe'],

            ['foundation_year', 'default', 'value' => 0],
            ['foundation_year', 'integer', 'min' => 0, 'max' => 9999],

        ];
    }

    public function articleExist($attribute, $params){
        if($this->$attribute == 0)
            return;

        if(Article::find()->where(['id' => $this->$attribute])->exists()) return;

        $this->addError($attribute, 'Статьи id' . $this->$attribute . ' не существует');
    }

    public function deleteByThisId()
    {
        if (!is_numeric($this->id)) return false;
        ProjectCategory::deleteAll(['project_id' => $this->id]);
        return static::deleteAll(['id' => $this->id]);
    }

}