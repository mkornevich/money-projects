<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/17/2017
 * Time: 8:05 PM
 */

namespace app\models\frontend;

use app\models\base\Category;
use app\models\base\ProjectCategory;

class FrontCategory extends Category
{
    const SCENARIO_NEW_FORM = 'new_form';
    const SCENARIO_EDIT_FORM = 'edit_form';
    const  SCENARIO_REMOVE_FORM = 'remove_form';

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[static::SCENARIO_REMOVE_FORM] = ['id'];

        $scenarios[static::SCENARIO_EDIT_FORM] =
            ['name', 'description', 'rating'];

        $scenarios[static::SCENARIO_NEW_FORM] =
            ['name', 'description', 'rating'];

        return $scenarios;
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя категории',
            'rating' => 'Рейтинг категории',
            'description' => 'Полное описание категории',
        ];
    }

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer', 'max' => 999999],

            ['name', 'required'],
            ['name', 'string', 'max' => 60],

            ['description', 'default', 'value' => 'Описание этой категории не заполнено'],
            ['description', 'string'],

            ['rating', 'default', 'value' => 1],
            ['rating', 'integer', 'max' => 255],
        ];
    }

    public function deleteByThisId()
    {
        if (!is_numeric($this->id)) return false;
        ProjectCategory::deleteAll(['category_id' => $this->id]);
        return static::deleteAll(['id' => $this->id]);
    }

}