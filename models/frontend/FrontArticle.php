<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/22/2017
 * Time: 12:49 PM
 */

namespace app\models\frontend;


use app\models\base\Article;

class FrontArticle extends Article
{
    const SCENARIO_NEW_FORM = 'new_form';
    const SCENARIO_EDIT_FORM = 'edit_form';
    const  SCENARIO_REMOVE_FORM = 'remove_form';

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[static::SCENARIO_REMOVE_FORM] = ['id'];
        $scenarios[static::SCENARIO_EDIT_FORM] = ['name', 'description', 'text', 'target_id', 'target_type', 'formattedCreatedAt', 'draft'];
        $scenarios[static::SCENARIO_NEW_FORM] = ['name', 'description', 'text', 'target_id', 'target_type', 'formattedCreatedAt', 'draft'];

        return $scenarios;
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя статьи',
            'text' => 'Текст статьи',
            'description' => 'Краткое описание',
            'target_id' => 'Цельевой id',
            'target_type' => 'Тип цельевого id',
            'formattedCreatedAt' => 'Дата публикации',
            'draft' => 'Сохранить как черновик',
        ];
    }

    public function validateName($attribute, $params)
    {

        $query = static::find()
            ->where(['name' => $this->$attribute,]);

        if($this->scenario == static::SCENARIO_EDIT_FORM)
            $query = $query->andWhere(['!=', 'id', $this->id]);

        if ($query->exists())
            $this->addError($attribute, 'Статья с таким именем уже есть.');
    }

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer', 'max' => 999999],

            ['name', 'required'],
            ['name', 'validateName'],
            ['name', 'string', 'max' => 60],

            ['description', 'default', 'value' => 'Краткое описание этой статьи не заполнено'],
            ['description', 'string', 'max' => 350],

            ['text', 'default', 'value' => 'Описание этой статьи не заполнено'],

            ['target_id', 'integer', 'max' => 999999],
            ['target_id', 'default', 'value' => 0],

            ['target_type', 'in', 'range' => [0, 1]],

            ['formattedCreatedAt', 'required'],
            ['formattedCreatedAt', 'date', 'format' => 'php:d.m.yy'],

            ['draft', 'boolean'],
        ];
    }


    public function deleteByThisId()
    {
        if (!is_numeric($this->id)) return false;
        return static::deleteAll(['id' => $this->id]);
    }
}