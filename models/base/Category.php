<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/4/2017
 * Time: 9:56 PM
 */

namespace app\models\base;

use app\components\category\CategoryFinder;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


class Category extends Model
{

    private static function getCategoryIdsByProject(Project $project)
    {

        $projectCategories = ProjectCategory::find()
            ->select(['project_id', 'category_id'])
            ->where(['project_id' => $project->id])
            ->asArray()->all();


        return ArrayHelper::getColumn($projectCategories, 'category_id');

    }

    public static function attachCategoryDataToProject(Project $project)
    {
        $project->categoryData = [];
        $rootCategories = [];

        $categoryIds = static::getCategoryIdsByProject($project);
        $categories = (new CategoryFinder())->getAllArrayCategories();

        foreach($categories as $category)
            if($category['parentId'] == 0)
                $rootCategories[] = $category;

        foreach ($rootCategories as $rootCategory){
            $item = [];
            $item['rootName'] = $rootCategory['name'];
            $item['names'] = [];

            foreach ($categories as $category)
                if($category['parentId'] == $rootCategory['id'] && in_array($category['id'], $categoryIds))
                    $item['names'][] = $category['name'];

            $project->categoryData[] = $item;
        }

    }


}