<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/7/2017
 * Time: 3:00 PM
 */

namespace app\models\base;


use yii\db\ActiveRecord;

/**
 * @property mixed category_id
 * @property mixed project_id
 * @property mixed id
 */
class ProjectCategory extends ActiveRecord
{

}