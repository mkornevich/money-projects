<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/22/2017
 * Time: 12:32 PM
 */

namespace app\models\base;


use Codeception\Util\Debug;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\log\Logger;

/**
 * @property int $id [int(11)]
 * @property string $name [varchar(535)]
 * @property string $text
 * @property int $target_id [int(11)]
 * @property bool $target_type [tinyint(11)]
 * @property string $description
 * @property int $created_at [int(11)]
 * @property int $updated_at [int(11)]
 * @property bool $draft [tinyint(1)]
 */
class Article extends ActiveRecord
{
    const TARGET_TYPE_CATEGORY = '0';
    const TARGET_TYPE_PROJECT = '1';

    public static function tableName()
    {
        return '{{%article}}';
    }


    // добавляет к массиву проектов описание
    public static function attachDescriptionToProjectsArray($projects)
    {
        $resultProjects = [];

        $articleIds = ArrayHelper::getColumn($projects, 'target_article');
        $articles = static::find()
            ->select(['id', 'description'])
            ->where(['id' => $articleIds])
            ->asArray()
            ->all();

        foreach ($projects as $project) {
            $resultProject = $project;
            $resultProject['description'] = null;

            if ($project['target_article'] == 0) {
                $resultProjects[] = $resultProject;
                continue;
            }

            foreach ($articles as $article) {
                if ($article['id'] == $project['target_article']) {
                    $resultProject['description'] = $article['description'];
                    break;
                }
            }

            $resultProjects[] = $resultProject;
        }


        return $resultProjects;
    }

    // добавляет к проекту текст статьи и описание
    public static function attachArticleTextAndDescriptionToProject(Project $project)
    {
        $article = Article::find()
            ->where(['id' => $project->target_article])
            ->one();

        if (!$article instanceof Article) return;

        $project->text = $article->text;
        $project->description = $article->description;
    }


    public function getFormattedCreatedAt()
    {
        if ($this->created_at == null)
            return Yii::$app->formatter->asDate(time(), 'php:d.m.yy');
        else
            return Yii::$app->formatter->asDate($this->created_at, 'php:d.m.yy');
    }

    public function setFormattedCreatedAt($value)
    {
        $this->created_at = strtotime($value);
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $this->updated_at = time();
        return parent::save($runValidation, $attributeNames);
    }

}