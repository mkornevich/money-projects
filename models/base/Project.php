<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/7/2017
 * Time: 10:05 AM
 */

namespace app\models\base;


use yii\db\ActiveRecord;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * @property $id
 * @property $name
 * @property $url
 * @property $rating
 * @property $foundation_date
 * @property mixed $selectCategory
 * @property mixed $formattedFoundationDate
 * @property int $target_article [int(6) unsigned]
 * @property int $foundation_year [int(4) unsigned]
 */
class Project extends ActiveRecord
{
    /**
     * @var array|Article
     */
    public $text = null;
    public $description = null;

    public $categoryData = null;

    private $selectCategory = null;

    public static function tableName()
    {
        return '{{%project}}';
    }

    /**
     * @param $ids
     * @return Query
     */
    public static function findByCategoriesId($ids)
    {

        $projectIds = ProjectCategory::find()
            ->select(['project_category.project_id'])
            ->from(['project_category', 'project'])
            ->where('project_category.project_id = project.id')
            ->andWhere(['project_category.category_id' => $ids])
            ->groupBy('project_category.project_id')
            ->having('COUNT(*) = :ids_count', [':ids_count' => count($ids)]);

        return static::find()->where(['id' => $projectIds]);

    }

    /**
     * @return mixed
     */
    public function getSelectCategory()
    {
        if ($this->selectCategory)
            return $this->selectCategory;
        else {
            $categories = ProjectCategory::find()
                ->select('category_id')
                ->where(['project_id' => $this->id])
                ->asArray()
                ->all();

            return ArrayHelper::getColumn($categories, 'category_id');
        }
    }

    public function setSelectCategory($value)
    {
        $this->selectCategory = $value;
    }

    // эта крутая функция )))))
    public function updateSelectCategory()
    {
        ProjectCategory::deleteAll(['project_id' => $this->id]);

        if (!is_array($this->selectCategory) or $this->selectCategory == []) return;

        $categoryColumnBuild = [];

        foreach ($this->selectCategory as $id) {
            $categoryColumnBuild[] = [
                'project_id' => $this->id,
                'category_id' => $id,
            ];
        }

        Yii::$app->db->createCommand()
            ->batchInsert(ProjectCategory::tableName(), ['project_id', 'category_id'], $categoryColumnBuild)
            ->execute();
    }



}