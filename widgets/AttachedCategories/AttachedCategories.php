<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 11/26/2017
 * Time: 2:24 PM
 */

namespace app\widgets\AttachedCategories;


use app\components\category\CategoryFinder;
use yii\base\Widget;

class AttachedCategories extends Widget
{

    public $model;

    public $attribute;

    private $categories;

    private function getChildrenByCategoryId($nodeId)
    {
        $result = [];

        foreach ($this->categories as $category)
            if ($category['parentId'] == $nodeId)
                $result[] = $category;

        if ($result === []) return false;

        return $result;
    }

    private function renderTreeNode($category)
    {
        $result = '';

        $checkBoxName = (new \ReflectionClass(get_class($this->model)))->getShortName() . '[' . $this->attribute . '][]';
        $checkBoxChecked = (in_array($category['id'], $this->model->{$this->attribute})) ? 'checked' : '';

        $checkBoxVisible = $category['checkBoxVisible'];

        $result .= '<div class="tree-item">';

        $result .= '<div class="tree-title">';
        if ($checkBoxVisible)
            $result .= '<input type="checkbox"' . $checkBoxChecked . ' name="' . $checkBoxName . '" value="' . $category['id'] . '">';
        $result .= ' ' . $category['name'];
        $result .= '</div>';


        if ($children = $this->getChildrenByCategoryId($category['id'])) {

            $result .= '<div class="tree-children">';

            foreach ($children as $child) {
                $result .= $this->renderTreeNode($child);
            }

            $result .= '</div>';
        }

        $result .= '</div>';

        return $result;

    }

    // start
    private function renderFullTree()
    {
        $result = '';

        $children = $this->getChildrenByCategoryId(0);

        foreach ($children as $category)
            $result .= $this->renderTreeNode($category);

        return $result;

    }

    public function run()
    {
        $this->categories = (new CategoryFinder())->getAllArrayCategories();

        return $this->render('main', [
            'fullTreeHtml' => $this->renderFullTree(),
        ]);
    }
}