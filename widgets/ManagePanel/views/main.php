<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 12/19/2017
 * Time: 9:42 PM
 *
 * @var array $permissions
 * @var bool $show
 *
 */
use yii\helpers\Url;

?>


<? if ($show): ?>

        <div class="title">Админ панель</div>
        <div class="sub-title">Действия</div>

        <div class="content-no-padding">

            <? if ($permissions['editAndAddProject']): ?>
                <a class="styled-item"
                   href="<?= Url::to(['project/new']) ?>">
                    <i class="glyphicon glyphicon-folder-close"></i>
                    <span>Добавить проект</span>
                </a>
            <? endif; ?>

            <? if ($permissions['editAndAddArticle']): ?>
                <a class="styled-item"
                   href="<?= Url::to(['article/new']) ?>">
                    <i class="glyphicon glyphicon-list-alt"></i>
                    <span>Добавить статью</span>
                </a>
            <? endif; ?>


        </div>



<? endif; ?>

