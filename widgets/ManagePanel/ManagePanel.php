<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 12/19/2017
 * Time: 9:39 PM
 */

namespace app\widgets\ManagePanel;


use Yii;
use yii\base\Widget;

class ManagePanel extends Widget
{
    private $permissions = [
        'editAndAddArticle',
        'editAndAddProject',
    ];

    private function getPreparePermissions()
    {
        $result = [];
        foreach ($this->permissions as $permissionString){
            $result[$permissionString] = Yii::$app->user->can($permissionString);
        }
        return $result;
    }

    private function existEnablePermission($permissions){
        foreach ($permissions as $permissionBool)
            if($permissionBool)
                return true;
        return false;
    }

    public function run()
    {
        $permissions = $this->getPreparePermissions();

        return $this->render('main', [
            'permissions' => $permissions,
            'show' => $this->existEnablePermission($permissions),
        ]);
    }

}