<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/25/2017
 * Time: 9:34 PM
 *
 * @var \app\models\base\Article[] $articles ;
 * @var string $type ; "project" | "category";
 * @var string $typeName ;
 */
use yii\helpers\Html;
use yii\helpers\Url;


?>


<? if ($articles): ?>

    <div class="sub-title">Рекомендованные статьи</div>


    <div class="content-no-padding" style="padding: 5px 5px 0.1px 5px">

        <? foreach ($articles as $article): ?>

            <div style="border: dashed 1px #d2d2d2; padding: 0 5px; margin-bottom: 5px">

                <div class="red font-bold"><?= Html::encode($article->name) ?></div>
                <div><?= Html::encode($article->description) ?></div>
                <div class="fix-overflow">
                <a href="<?= Url::to(['article/view', 'id' => $article->id]) ?>"
                   class="font-size-1 pull-right">
                    Читать полностью
                </a>
                </div>






            </div>


        <? endforeach; ?>

    </div>

<? endif; ?>

