<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 10/25/2017
 * Time: 9:17 PM
 */

namespace app\widgets\SuggestedArticles;


use app\models\base\Article;
use app\models\base\Category;
use app\models\base\Project;
use app\models\base\ProjectCategory;
use yii\base\Widget;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 *
 * @property void $targetType
 */
class SuggestedArticles extends Widget
{

    /**
     * @var Project | Category;
     */
    public $data;





    public function getSuggestedArticlesByProject(Project $project)
    {

        return Article::find()
            ->where([
                'target_id' => $project->id,
                'target_type' => Article::TARGET_TYPE_PROJECT,
            ])->limit(7)->all();
    }

    public function getSuggestedArticlesByArticle(Article $article)
    {

        if($article->target_type != Article::TARGET_TYPE_PROJECT) return [];
        if($article->target_id == 0) return [];

        return Article::find()
            ->where([
                'target_id' => $article->target_id,
                'target_type' => Article::TARGET_TYPE_PROJECT,
            ])->andWhere(['!=', 'id', $article->id])
            ->limit(7)->all();
    }


    private function renderProject(Project $project)
    {
        return $this->render('articles', [
            'type' => 'project',
            'articles' => $this->getSuggestedArticlesByProject($project)
        ]);
    }

    private function renderArticle(Article $article)
    {
        return $this->render('articles', [
            'type' => 'article',
            'articles' => $this->getSuggestedArticlesByArticle($article)
        ]);
    }

    public function run()
    {

        if (!is_object($this->data))
            throw new Exception('$data не является обьектом');

        elseif ($this->data instanceof Project)
            return $this->renderProject($this->data);

        elseif ($this->data instanceof Article)
            return $this->renderArticle($this->data);

        else
            throw new Exception('Неверно задана $data');
    }
}