<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 12/28/2017
 * Time: 6:14 PM
 */

namespace app\widgets\InfoPanel;


use yii\base\Widget;

class InfoPanel extends Widget
{
    public function run()
    {
        return $this->render('main');
    }
}