<?php
/**
 * Created by PhpStorm.
 * User: mkornevich
 * Date: 12/28/2017
 * Time: 6:15 PM
 */
use yii\helpers\Html;

?>


<div class="title">Информационная панель</div>


<div class="sub-title">ВК группа</div>

<div class="content">
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?151"></script>

    <!-- VK Widget -->
    <div id="vk_groups"></div>
    <script type="text/javascript">
        VK.Widgets.Group("vk_groups", {mode: 4, width: "270", height: "400"}, 159209527);
    </script>
</div>

<div class="sub-title">Может быть интересно</div>

<div class="content">
    <script language="JavaScript" charset="UTF-8" src="http://z1520.takru.com/in.php?id=1524090"></script>
</div>

<div class="sub-title">Дополнительно</div>

<div class="content text-center">
        <span class="font-size-1 grey ">
        <i class="glyphicon glyphicon-copyright-mark"></i>
            <?= Html::encode(Yii::$app->params['domainName']) ?>
            <span>2017-<?= date('Y') ?></span>
        </span>
</div>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter47199732 = new Ya.Metrika({
                    id: 47199732,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/47199732" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->


