<?php
// test
$_ENV = [
    'debug' => true,

    //database
    'db' => [
        'dsn' => 'mysql:host=localhost;dbname=top-money',
        'username' => 'root',
        'password' => '',
    ],

    'adminEmail' => 'admin@example.com',
    'domainName' => 'money-catalog.com',

    'storagePath' => '@app/web/storage/',
    'storageUrl' => 'http://top-money.local/storage/',


];