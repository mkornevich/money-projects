<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => $_ENV['db']['dsn'],
    'username' => $_ENV['db']['username'],
    'password' => $_ENV['db']['password'],
    'charset' => 'utf8',
];
