<?php

use yii\helpers\Url;

return [
    'adminEmail' => $_ENV['adminEmail'],
    'domainName' => $_ENV['domainName'],
    'imperaviEditorPlugins' => [
        'clips',
        'fullscreen',
        'imagemanager',
        'fontcolor',
        'table',
        'fontsize'
    ],
];
